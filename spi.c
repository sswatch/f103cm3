/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright (C) 2013 Stephen Dwyer <scdwyer@ualberta.ca>
 * Copyright (C) 2014 Laurent Barattero <laurentba@larueluberlu.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "delay.h"
#include "lcd.h"
#include "widget_time.h"
#include "widget.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rtc.h>


static void clock_setup ( void )
{
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
    rtc_auto_awake(RCC_LSE, 0x7fff);
    init_delay();

}

static void init_gpio ( void )
{
    rcc_periph_clock_enable ( RCC_GPIOB );
    gpio_set_mode ( GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12 );
    gpio_set ( GPIOB, GPIO12 );
}


int main ( void )
{


    clock_setup();
    init_gpio();
    lcd_init();

    gpio_clear ( GPIOB, GPIO12 );
    
//     uint8_t c = 0;
//     uint8_t x = 0;
//     font_t * font = &font_8x16;
    wt_init();
    while ( 1 ) {
//         lcd_clear();
//         
//         lcd_blit ( x, 32, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*c);
//         lcd_flush();
//                 
        widget_loop();
        //delay ( 500 );
        //gpio_toggle ( GPIOB, GPIO12 );
//         c++;
//         x+=3;
//         if ( c>=10 ) c = 0;
//         if(x>=128-font->w){
//           x = 0;
//         }
    }
    return 0;
}
