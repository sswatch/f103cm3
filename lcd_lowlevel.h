#pragma once

#include <stdint.h>
#include <stdbool.h>

void lcd_clear ( void );
void lcd_fill ( bool color );
void lcd_putpixel ( uint8_t x, uint8_t y, bool color );
void lcd_blit ( uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t * buf );
void lcd_flush ( void );
void lcd_flush_segment(uint8_t x, uint8_t y, uint8_t w, uint8_t h);

