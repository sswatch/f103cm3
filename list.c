#include "list.h"
#include <stdlib.h>


struct list_t * list_add_node ( struct list_t * head ) {
    if ( !head ) return 0;
    struct list_t * last = head;
    while ( last->next ) {
        last = last->next;
    }
    struct list_t * ret = ( struct list_t * ) malloc ( sizeof ( struct list_t ) );

    return ret;
}
void list_remove_node ( struct list_t * head, struct list_t * node ) {
    if ( !head||!node ) {
        return;
    }
    struct list_t ** cur = &head;
    while ( ( ( *cur ) != node ) && ( *cur!=0 ) ) {
        cur = & ( *cur )->next;
    }
    if ( *cur!=0 ) {
        return;
    }
    
    *cur = node->next;
    free ( node );
}

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
