#pragma once


#include <stdint.h>
#include <stddef.h>

void lcd_io_init(void);

void lcd_reset(void);
void lcd_write_command(uint8_t cmd);
void lcd_write_data(uint8_t * data, size_t size);
