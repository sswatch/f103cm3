#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>
#include "delay.h"



static uint32_t _systick_cnt = 0;

void sys_tick_handler(void)
{
	_systick_cnt++;
}


void delay(uint32_t ms){
    uint32_t tf = _systick_cnt + ms;
    while((tf - _systick_cnt)>0){
        __asm__("nop");
    }
}

void init_delay(void){
    /* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	/* SysTick interrupt every N clock pulses: set reload to N-1 */
	systick_set_reload(8999);

	systick_interrupt_enable();

	/* Start counting. */
	systick_counter_enable();
}
