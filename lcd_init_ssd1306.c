#include "lcd_init.h"

#include "lcd_io.h"
#include "lcd_lowlevel.h"
#include "delay.h"



void lcd_init ( void )
{
    lcd_io_init();
    // Reset OLED
	lcd_reset();

    // Wait for the screen to boot
    delay(100);
    
    // Init OLED
    lcd_write_command(0xAE); //display off

    lcd_write_command(0x20); //Set Memory Addressing Mode   
    lcd_write_command(0x10); // 00,Horizontal Addressing Mode; 01,Vertical Addressing Mode;
                                // 10,Page Addressing Mode (RESET); 11,Invalid

    lcd_write_command(0xB0); //Set Page Start Address for Page Addressing Mode,0-7


    lcd_write_command(0xC8); //Set COM Output Scan Direction


    lcd_write_command(0x00); //---set low column address
    lcd_write_command(0x10); //---set high column address

    lcd_write_command(0x40); //--set start line address - CHECK

    lcd_write_command(0x81); //--set contrast control register - CHECK
    lcd_write_command(0x05);


    lcd_write_command(0xA1); //--set segment re-map 0 to 127 - CHECK



    lcd_write_command(0xA6); //--set normal color


    lcd_write_command(0xA8); //--set multiplex ratio(1 to 64) - CHECK
    lcd_write_command(0x3F); //

    lcd_write_command(0xA4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content

    lcd_write_command(0xD3); //-set display offset - CHECK
    lcd_write_command(0x00); //-not offset

    lcd_write_command(0xD5); //--set display clock divide ratio/oscillator frequency
    lcd_write_command(0xF0); //--set divide ratio

    lcd_write_command(0xD9); //--set pre-charge period
    lcd_write_command(0x22); //

    lcd_write_command(0xDA); //--set com pins hardware configuration - CHECK
    lcd_write_command(0x12);

    lcd_write_command(0xDB); //--set vcomh
    lcd_write_command(0x20); //0x20,0.77xVcc

    lcd_write_command(0x8D); //--set DC-DC enable
    lcd_write_command(0x14); //
    lcd_write_command(0xAF); //--turn on SSD1306 panel

    uint8_t i = 0;
    while(i<3){
        lcd_fill(true);
        lcd_flush();        
        delay(100);
        
        lcd_fill(false);
        lcd_flush();
        delay(200);
        
        i++;
    }
    lcd_flush();
//     // Clear screen
//     ssd1306_Fill(Black);
//     
//     // Flush buffer to screen
//     ssd1306_UpdateScreen();
//     
//     // Set default values for screen object
//     SSD1306.CurrentX = 0;
//     SSD1306.CurrentY = 0;
//     
// SSD1306.Initialized = 1;
}
