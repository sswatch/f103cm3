#include "widget.h"
#include "list.h"
#include <stdlib.h>


static struct list_t _wid_list = {
    .data = 0,
    .next = 0,
};


static widget_t * _wid_alloc_node ( struct list_t * node ) {
    if ( node ) {
        node->data = malloc ( sizeof ( widget_t ) );
        return node->data;
    }
    return 0;
}

widget_t * widget_create(void) {
    if ( !_wid_list.data ) {
        return _wid_alloc_node ( &_wid_list );
    }
    struct list_t * node = list_add_node ( &_wid_list );
    if ( node ) {
        _wid_alloc_node ( node );
    }
    return 0;
}

void widget_remove ( widget_t * w ) {
    if ( !w ) return;
    struct list_t * cur = &_wid_list;
    while ( cur ) {
        if ( cur->data==w ) {
            list_remove_node ( &_wid_list, cur );
        }
        cur = cur->next;
    }
    free ( w );
}

void widget_set_update_callback ( widget_t* w, void ( *cb ) ( widget_t* ) ) {
    if(!w){
        return;
    }
    w->update_callback = cb;
}

void widget_run_update_callback ( widget_t* w ) {
    if(!w){
        return;
    }
    if(!w->update_callback){
        return;
    }
    w->update_callback(w);
}


void widget_loop(void)
{
    struct list_t* cur = &_wid_list;
    while(cur){
        widget_run_update_callback(cur->data);
        cur = cur->next;
    }
}



// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
