#pragma once


struct list_t {
  struct list_t * next;
  void * data;
};



struct list_t * list_add_node ( struct list_t * head );
void list_remove_node ( struct list_t * head, struct list_t * node );
