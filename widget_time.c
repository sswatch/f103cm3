#include "widget_time.h"
#include <libopencm3/stm32/rtc.h>
#include <libopencm3/stm32/gpio.h>
#include "widget.h"
#include "lcd.h"

uint32_t prev_t;

static void _wt_update ( widget_t * w ) {
    if(!w){
        return;
    }
    
    
    
     uint32_t cnt = rtc_get_counter_val();
     if(cnt==prev_t){
         return;
     }
     uint8_t h1, h2, m1, m2, s1, s2;
     uint8_t h = (cnt/2400)%24;
     h1 = h/10;
     h2 = h%10;
     lcd_blit ( w->x, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*h1);
     lcd_blit ( w->x+8, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*h2);
     
     uint8_t m = (cnt/60)%60;
     m1 = m/10;
     m2 = m%10;
     lcd_blit ( w->x + 24, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*m1);
     lcd_blit ( w->x+32, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*m2);
     
     uint8_t s = cnt%60;
     s1 = s/10;
     s2 = s%10;
     
     lcd_blit ( w->x + 48, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*s1);
     lcd_blit ( w->x+56, w->y, font_8x16.w, font_8x16.h, font_8x16.digit+font_8x16.bps*s2);
     lcd_flush();
     prev_t = cnt;
     gpio_toggle(GPIOB, GPIO12);
}


void wt_init ( void ) {
    widget_t * w = widget_create();
    w->x = 32;
    w->y = 0;
    w->w = 8*8;
    w->h = 16;
    widget_set_update_callback ( w, _wt_update );
}
// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
