#include "lcd_lowlevel.h"
#include "lcd_io.h"

#define LCD_WIDTH 128
#define LCD_HEIGHT 64


static uint8_t _lcd_buf[ ( LCD_HEIGHT*LCD_WIDTH ) /8];

void lcd_clear()
{
    lcd_fill ( false );
}
void lcd_fill ( bool color )
{
    uint32_t pg_max = ( LCD_HEIGHT*LCD_WIDTH ) /8;
    uint32_t i = 0;
    uint8_t color8 = color?0xff:0x00;
    while ( i<pg_max ) {
        _lcd_buf[i] = color8;
        i++;
    }
}
void lcd_putpixel ( uint8_t x, uint8_t y, bool color )
{
    if(x>=LCD_WIDTH||y>=LCD_HEIGHT){
        return;
    }
    uint32_t dst_bitp = x+(y/8)*LCD_WIDTH;
    
    
    if ( color ) {
        _lcd_buf[dst_bitp] |= 1 << (y % 8);
    } else {
        _lcd_buf[dst_bitp] &= ~ (1 << (y % 8));
    }
}
void lcd_blit ( uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t * buf )
{
    uint8_t i = 0;//номер строки в buf
    while ( i<h ) {
        uint8_t j = 0;//номер столбца в buf
        if ( i+y>LCD_HEIGHT ) {
            break;
        }
        while ( j<w ) {
            if ( j+x>LCD_WIDTH ) {
                break;
            }
            uint32_t x_pos, y_pos, x_pos_bmp, y_pos_bmp;
            x_pos = j+x;
            y_pos = i+y;
            x_pos_bmp = j;
            y_pos_bmp = i;
            uint32_t src_bitp = x_pos_bmp + y_pos_bmp*w;
            uint32_t src_byte_off = src_bitp/8;
            uint8_t src_bit_off = src_bitp%8;
            bool pxv = ( buf[src_byte_off]& ( 1<<src_bit_off ) ) !=0;
            lcd_putpixel(x_pos, y_pos, pxv);
            j++;
        }

        i++;
    }
}
void lcd_flush ( void )
{
    uint8_t i;
    for ( i = 0; i < LCD_HEIGHT/8; i++ ) {
        lcd_write_command ( 0xB0 + i );
        lcd_write_command ( 0x00 );
        lcd_write_command ( 0x10 );
        lcd_write_data ( &_lcd_buf[(LCD_WIDTH*i)],LCD_WIDTH );
    }
}

void lcd_flush_segment(uint8_t x, uint8_t y, uint8_t w, uint8_t h){
   uint8_t i;
   uint8_t hmax = y+h;
    for ( i = y/8; i*8 < hmax; i++ ) {
        lcd_write_command ( 0xB0 + i );
        lcd_write_command ( 0x00 );
        lcd_write_command ( 0x10 );
        lcd_write_data ( &_lcd_buf[(LCD_WIDTH*i)],LCD_WIDTH );
    } 
}

