#include "lcd_io.h"
#include "delay.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>


#define LCD_IO_SPI SPI1
#define LCD_IO_DC GPIO6
#define LCD_IO_MOSI GPIO7
#define LCD_IO_SCK GPIO5
#define LCD_IO_CS GPIO4
#define LCD_IO_RS GPIO3
#define LCD_IO_GPIO GPIOA


#define LCD_RCC_GPIO RCC_GPIOA
#define LCD_RCC_SPI  RCC_SPI1



void lcd_io_init ( void )
{

    rcc_periph_clock_enable ( LCD_RCC_GPIO );
    rcc_periph_clock_enable ( RCC_AFIO );
     rcc_periph_clock_enable ( LCD_RCC_SPI );
// 
// 
//     /* Configure GPIOs: SS=PA4, SCK=PA5, -MISO=PA6- and MOSI=PA7 */
    gpio_set_mode ( LCD_IO_GPIO, GPIO_MODE_OUTPUT_50_MHZ,
                    GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, LCD_IO_SCK | //LCD_IO_CS | 
                    LCD_IO_MOSI );
// 
    gpio_set_mode ( LCD_IO_GPIO, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL,
                    LCD_IO_CS |LCD_IO_DC|LCD_IO_RS);
// 
//     /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
    spi_reset ( LCD_RCC_SPI );
// 
//     /* Set up SPI in Master mode with:
//      * Clock baud rate: 1/64 of peripheral clock frequency
//      * Clock polarity: Idle High
//      * Clock phase: Data valid on 2nd clock pulse
//      * Data frame format: 8-bit
//      * Frame format: MSB First
//      */
//     spi_init_master(SPI1, SPI_CR1_BAUDRATE_FPCLK_DIV_128, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
    spi_init_master ( LCD_IO_SPI, SPI_CR1_BAUDRATE_FPCLK_DIV_2, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                      SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST );
// 
//     /*
//      * Set NSS management to software.
//      *
//      * Note:
//      * Setting nss high is very important, even if we are controlling the GPIO
//      * ourselves this bit needs to be at least set to 1, otherwise the spi
//      * peripheral will not send any data out.
//      */
    spi_enable_software_slave_management(LCD_IO_SPI);
    spi_set_bidirectional_transmit_only_mode(LCD_IO_SPI);
    spi_set_nss_high(LCD_IO_SPI);
     //spi_enable_software_slave_management ( LCD_RCC_SPI );
     //spi_set_nss_high ( LCD_RCC_SPI );
// 
//     /* Enable SPI1 periph. */
//      spi_enable ( LCD_IO_SPI );
    spi_enable(LCD_IO_SPI);
    
    
//     SPI_CR1(SPI1) |= SPI_CR1_SPE;
}


void lcd_reset()
{
    gpio_set ( LCD_IO_GPIO, LCD_IO_CS );
    // Reset the OLED
//     spi_set_nss_high(SPI1)
    gpio_clear ( LCD_IO_GPIO, LCD_IO_RS );
    delay ( 10 );
    gpio_set ( LCD_IO_GPIO, LCD_IO_RS );
    delay ( 10 );
}


void lcd_write_command ( uint8_t cmd )
{
    //spi_set_nss_high(LCD_RCC_SPI);
    gpio_clear ( LCD_IO_GPIO, LCD_IO_CS );
    gpio_clear ( LCD_IO_GPIO, LCD_IO_DC );

    spi_send ( LCD_IO_SPI, cmd );

    gpio_set ( LCD_IO_GPIO, LCD_IO_CS );
}
void lcd_write_data ( uint8_t * data, size_t size )
{
    size_t i = 0;
    //uint16_t * data16 = ( uint16_t* ) data;

    gpio_clear ( LCD_IO_GPIO, LCD_IO_CS );
    gpio_set ( LCD_IO_GPIO, LCD_IO_DC );

    while ( i<size ) {
        spi_send ( LCD_IO_SPI, data[i] );
        i++;
    }

    gpio_set ( LCD_IO_GPIO, LCD_IO_CS );
}

