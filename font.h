#include <stdint.h>


typedef struct font_t{
  uint8_t * digit;
  uint8_t * a_big;
  uint8_t * a_small;
  uint8_t w;
  uint8_t h;
  uint8_t bps;
} font_t;


extern font_t font_8x16;

