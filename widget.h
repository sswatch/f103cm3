#pragma once
#include <stdint.h>


typedef struct widget_t {
    uint8_t x;
    uint8_t y;
    uint8_t w;
    uint8_t h;
    void ( *update_callback ) ( struct widget_t* );
} widget_t;


widget_t * widget_create(void);
void widget_remove ( widget_t * w );

void widget_set_update_callback ( widget_t* w, void ( *cb ) ( widget_t* ) );
void widget_run_update_callback ( widget_t* w );

void widget_loop(void);
// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
